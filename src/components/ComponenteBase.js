import React from 'react';
import './styles.css';


const ComponenteBase = props =>{

    const {encabezado, cuerpo} = props;

    return <div className="bloqueNoticia">
                <h3>{`Noticia : ${encabezado}`}</h3>
                <p>{`Articulo : ${cuerpo}`}</p>
            </div>
    
}

export default ComponenteBase;