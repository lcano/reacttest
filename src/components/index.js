import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import getUrlWeatherByCity from './../Services/getUrlWeatherByCity';
import transformWeather from './../Services/transformWeather';
import Location from './Location';
import WeatherData from './WeatherLocation/WeatherData';
import './styles.css';




class WeatherLocation extends Component {

    constructor (props) {
        super (props);

        const {city} = props;

        this.state = {
            city,
            data: null,
        };
    };
    
    componentDidMount() {
        this.handleUpdateClick();
    }
    

    

    handleUpdateClick = () => {

        const api_weather = getUrlWeatherByCity(this.state.city);
        fetch(api_weather).then( resolve => {

            return resolve.json();
        }).then(data => {
            console.log("Resultado del handleUpdateClick")
            const newWeather =transformWeather(data);
            console.log(newWeather);
            // debugger;

            this.setState({
                data: newWeather
            })
        });
    };

    render () 
    {

        const {onWeatherLocationClick} = this.props;
        const {city, data } = this.state;
        
        return (
            <div className="weatherLocationCont" onClick = {onWeatherLocationClick}  >
            <Location city={city}></Location> 
            {data ?
                <WeatherData data ={data}></WeatherData>:
            <CircularProgress size={50}/>
            }
            </div>)
    };
};




export default WeatherLocation;

