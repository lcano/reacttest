import React from 'react';
import ComponenteBase from './ComponenteBase';



const ComponentePresenta  = ()=> {

      const noticias = [
        {noticia:"Noticia 1", cuerpo:"Cuerpo de noticia 1"},
         {noticia:"Noticia 2", cuerpo:"Cuerpo de noticia 2"},
        {noticia:"Noticia 3", cuerpo:"Cuerpo de noticia 3"},
        {noticia:"Noticia 4", cuerpo:"Cuerpo de noticia 4"},
        {noticia:"Noticia 5", cuerpo:"Cuerpo de noticia 5"}
      ]


const lecturaCiudades = noticias =>(
    noticias.map((noticia, index) =>
    (
        <ComponenteBase
            key={index}
            encabezado={noticia.noticia}
            cuerpo={noticia.cuerpo}>
          </ComponenteBase>
        ))
    );

        
return (<div> {lecturaCiudades(noticias)}
        </div>);
};

export default ComponentePresenta;