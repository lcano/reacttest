import React, {Component} from 'react';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import {Grid, Row, Col} from 'react-flexbox-grid';
import LocationList from './components/LocationList';
import ComponenteRender from './components/ComponenteRender';
import ComponentePresenta from './components/ComponentePresenta';
import './App.css';



const cities = [
  "Buenos Aires, ar",
  "Bogota, col",
  "Madrid, es",
  "Leon, es",
  "Ciudad de México, mx",

]

class App extends Component {



  render() {
    return (

            <Grid>
              <Row>
                  <AppBar position='sticky'>
                    <Toolbar>
                       <Typography variant="title" color ="inherit" >
                          Todo list
                       </Typography>
                    </Toolbar>
                  </AppBar>
              </Row>

                <Col>
                  {/* {<LocationList 
                    cities = {cities} 
                    onSelectedLocation = {this.handleSelectedLocation}>
                  </LocationList> } */}
                  <ComponentePresenta/>

                </Col>
                

               

              <div className="App">

              </div> 
            </Grid>


    );
  }
}

export default App;
